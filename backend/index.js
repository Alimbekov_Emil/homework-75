const express = require("express");
const Vigenere = require("caesar-salad").Vigenere;
const cors = require("cors");
const app = express();

app.use(express.json());
app.use(cors());

const port = 8000;

app.post("/encode", (req, res) => {
  const encode = Vigenere.Cipher(req.body.password).crypt(req.body.message);
  res.send({ encoded: encode });
});

app.post("/decode", (req, res) => {
  const decode = Vigenere.Decipher(req.body.password).crypt(req.body.message);
  res.send({ decoded: decode });
});

app.listen(port, () => {
  console.log("We are live a port " + port);
});
