import axiosCipher from "../axiosCipher";

export const POST_DECODE_REQUEST = "POST_DECODE_REQUEST";
export const POST_DECODE_FAILURE = "POST_DECODE_FAILURE";
export const POST_DECODE_SUCCESS = "POST_DECODE_SUCCESS";

export const POST_ENCODE_REQUEST = "POST_ENCODE_REQUEST";
export const POST_ENCODE_FAILURE = "POST_ENCODE_FAILURE";
export const POST_ENCODE_SUCCESS = "POST_ENCODE_SUCCESS";

export const postDecodeRequest = () => ({ type: POST_DECODE_REQUEST });
export const postDecodeSuccess = (decode) => ({ type: POST_DECODE_SUCCESS, decode });
export const postDecodeFailure = (error) => ({ type: POST_DECODE_FAILURE, error });

export const postEncodeRequest = () => ({ type: POST_ENCODE_REQUEST });
export const postEncodeSuccess = (encode) => ({ type: POST_ENCODE_SUCCESS, encode });
export const postEncodeFailure = (error) => ({ type: POST_ENCODE_FAILURE, error });

export const postEncode = (message, password) => {
  return async (dispatch) => {
    try {
      dispatch(postEncodeRequest());
      const response = await axiosCipher.post("/encode", { message, password });
      dispatch(postEncodeSuccess(response.data.encoded));
    } catch (e) {
      dispatch(postEncodeFailure(e));
    }
  };
};

export const postDecode = (message, password) => {
  return async (dispatch) => {
    try {
      dispatch(postDecodeRequest());
      const response = await axiosCipher.post("/decode", { message, password });
      dispatch(postDecodeSuccess(response.data.decoded));
    } catch (e) {
      dispatch(postDecodeFailure(e));
    }
  };
};
