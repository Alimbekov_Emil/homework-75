import {
  POST_DECODE_FAILURE,
  POST_DECODE_REQUEST,
  POST_DECODE_SUCCESS,
  POST_ENCODE_FAILURE,
  POST_ENCODE_REQUEST,
  POST_ENCODE_SUCCESS,
} from "./actions";

const initialState = {
  error: false,
  loading: false,
  encode: "",
  decode: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_DECODE_REQUEST:
      return { ...state, loading: true };
    case POST_DECODE_SUCCESS:
      return { ...state, loading: false, encode: action.decode };
    case POST_DECODE_FAILURE:
      return { ...state, loading: false, error: action.error };
    case POST_ENCODE_REQUEST:
      return { ...state, loading: true };
    case POST_ENCODE_SUCCESS:
      return { ...state, loading: false, decode: action.encode };
    case POST_ENCODE_FAILURE:
      return { ...state, loading: false, error: action.error };
    default:
      return state;
  }
};

export default reducer;
