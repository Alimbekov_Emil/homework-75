import { useDispatch, useSelector } from "react-redux";
import { React, useState, useEffect } from "react";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import {
  Input,
  FormControl,
  Button,
  TextareaAutosize,
  Container,
  Box,
  CircularProgress,
} from "@material-ui/core";
import { postDecode, postEncode } from "./store/actions";

const App = () => {
  const dispatch = useDispatch();

  const encode = useSelector((state) => state.encode);
  const decode = useSelector((state) => state.decode);
  const loading = useSelector((state) => state.loading);

  const [cipher, setCipher] = useState({
    encode: "",
    password: "",
    decode: "",
  });

  useEffect(() => {
    setCipher((prevState) => ({
      ...prevState,
      encode,
    }));
  }, [encode]);

  useEffect(() => {
    setCipher((prevState) => ({
      ...prevState,
      decode,
    }));
  }, [decode]);

  const cipherDataChage = (event) => {
    const { name, value } = event.target;

    setCipher((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const decodeHandler = () => {
    if (cipher.password !== "") {
      dispatch(postDecode(cipher.decode, cipher.password));
    }
  };

  const encodeHandler = () => {
    if (cipher.password !== "" || cipher.encode !== encode) {
      dispatch(postEncode(cipher.encode, cipher.password));
    }
  };

  return (
    <Container maxWidth="sm">
      <FormControl>
        <TextareaAutosize
          cols="70"
          rows="10"
          name="encode"
          placeholder="Encode"
          className="Input"
          value={cipher.encode}
          onChange={cipherDataChage}
        />
        <Box display="block">
          <Input
            name="password"
            placeholder="Password"
            className="Input"
            value={cipher.password}
            onChange={cipherDataChage}
          />
          <Button color="primary" onClick={encodeHandler}>
            <ArrowDownwardIcon />
          </Button>
          <Button color="secondary" onClick={decodeHandler}>
            <ArrowUpwardIcon />
          </Button>
          {loading ? <CircularProgress /> : null}
        </Box>

        <TextareaAutosize
          cols="70"
          rows="10"
          name="decode"
          placeholder="Decode"
          className="Input"
          value={cipher.decode}
          onChange={cipherDataChage}
        />
      </FormControl>
    </Container>
  );
};

export default App;
